﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleClassConlsole
{
    class Airplane
    {
            protected string StartCity;
            protected string FinishCity;
            protected Date StartDate;
            protected Date FinishDate;
            public Airplane()
            {
                StartDate = new Date();
                FinishDate = new Date();
            }

            public Airplane(string startcity, string finishcity, int year, int month, int day, int hours, int minutes, int yearfinish, int monthfinish, int dayfinish, int hoursfinish, int minutesfinish)
            {
                StartCity = startcity;
                FinishCity = finishcity;
                StartDate = new Date(year, month, day, hours, minutes);
                FinishDate = new Date(yearfinish, monthfinish, dayfinish, hoursfinish, minutesfinish);
            }
            public Airplane(string startcity, string finishcity)
            {
            StartCity = startcity;
            FinishCity = finishcity;
            }
            public Airplane(Airplane obj)
            {
            StartCity = obj.StartCity;
            FinishCity = obj.FinishCity;
            StartDate = new Date(obj.StartDate);
            FinishDate = new Date(obj.FinishDate);
            }   
            public void SetStartCity(string startcity)
            {
                StartCity = startcity;
            }
            public string GetStartCity()
            {
                return StartCity;
            }
            public void SetFinishCity(string finishcity)
            {
                FinishCity = finishcity;
            }
            public string GetFinishCity()
            {
                return FinishCity;
            }
            public void SetYear(int year)
            {
                StartDate.SetYear(year);
            }
            public int GetYear()
            {
                return StartDate.GetYear();
            }
            public void SetMonth(int month)
            {
                StartDate.SetMonth(month);
            }
            public int GetMonth()
            {
                return StartDate.GetMonth();
            }
            public void SetDay(int day)
            {
                StartDate.SetDay(day);
            }
            public int GetDay()
            {
                return StartDate.GetDay();
            }
            public void SetHours(int hours)
            {
                StartDate.SetHours(hours);
            }
            public int GetHours()
            {
                return StartDate.GetHours();
            }
            public void SetMinutes(int minutes)
            {
                StartDate.SetMinutes(minutes);
            }
            public int GetMinutes()
            {
                return StartDate.GetMinutes();
            }
            public void SetYearFinish(int year)
            {
                FinishDate.SetYear(year);
            }
            public int GetYearFinish()
            {
                return FinishDate.GetYear();
            }
            public void SetMonthFinish(int month)
            {
                FinishDate.SetMonth(month);
            }
            public int GetMonthFinish()
            {
                return FinishDate.GetMonth();
            }
            public void SetDayFinish(int day)
            {
                FinishDate.SetDay(day);
            }
            public int GetDayFinish()
            {
                return FinishDate.GetDay();
            }
            public void SetHoursFinish(int hours)
            {
                FinishDate.SetHours(hours);
            }
            public int GetHoursFinish()
            {
                return FinishDate.GetHours();
            }
            public void SetMinutesFinish(int minutes)
            {
                FinishDate.SetMinutes(minutes);
            }
            public int GetMinutesFinish()
            {
                return FinishDate.GetMinutes();
            }
            public double GetTotalTime(int yearstart, int monthstart, int daystart, int hoursstart, int minutesstart, int yearfinish, int monthfinish, int dayfinish, int hoursfinish, int minutesfinish)
            {
            double result = (yearfinish - yearstart) * 365 * 24 * 60;
            result += (monthfinish - monthstart) * 365 / 12 * 24 * 60;
            result += (dayfinish - daystart) * 24 * 60;
            result += (hoursfinish - hoursstart) * 60;
            result += minutesfinish - minutesstart;
            return result;
            }
            public double GetTime(int yearstart, int monthstart, int daystart, int hoursstart, int minutesstart)
            {
            double result = yearstart * 365 * 24 * 60;
            result += monthstart * 365 / 12 * 24 * 60;
            result += daystart * 24 * 60;
            result += hoursstart * 60;
            result += minutesstart;
            return result;
            }
            public bool IsArrivingToday(int yearstart, int monthstart, int daystart, int yearfinish, int monthfinish, int dayfinish)
             {
            if ((yearstart == yearfinish) && (monthstart == monthfinish) && (daystart == dayfinish))
                return true;
            else
                return false;
             }
    }
}